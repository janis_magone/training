package org.training.core.attributes;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import org.training.core.model.EcentaNotificationModel;

public class notificationB2BUnitHandler implements DynamicAttributeHandler<B2BUnitModel, EcentaNotificationModel> {
    @Override
    public B2BUnitModel get(EcentaNotificationModel model) {
        return model.getB2bCustomer().getDefaultB2BUnit();
    }

    @Override
    public void set(EcentaNotificationModel model, B2BUnitModel b2BUnitModel) {
        throw new UnsupportedOperationException();
    }
}
