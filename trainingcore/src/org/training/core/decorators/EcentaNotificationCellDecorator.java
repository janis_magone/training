package org.training.core.decorators;

import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;

public class EcentaNotificationCellDecorator implements CSVCellDecorator {

    @Override
    public String decorate(final int position, Map<Integer, String> srcLine) {
        final String csvCell = srcLine.get(position);

        return csvCell + " Sample data";
    }
}
