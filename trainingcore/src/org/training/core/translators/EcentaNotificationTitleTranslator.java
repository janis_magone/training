package org.training.core.translators;


import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.security.JaloSecurityException;
import org.apache.commons.lang.StringUtils;
import org.training.core.jalo.EcentaNotification;
import org.training.core.model.EcentaNotificationModel;


public class EcentaNotificationTitleTranslator extends AbstractValueTranslator {

/*    protected EcentaNotification getEcentaNotification() {
        String NOTIFICATION_SERVICE_BEAN = "EcentaNotification";
        return (EcentaNotification) Registry.getApplicationContext().getBean(NOTIFICATION_SERVICE_BEAN);
    }*/

    @Override
    public Object importValue(String titleText, Item item) throws JaloInvalidParameterException {
        clearStatus();
        if (StringUtils.isBlank(titleText)) {

            String messageText = ((EcentaNotification) item).getMessage();
            //String messageText = this.getColumnDescriptor().getHeader().getDescriptorData().getModifier("message");
            //String messageText = this.getColumnDescriptor().getHeader().getReader().;
            //String messageText = null;
            try {
                messageText = item.getAllAttributes().get("message").toString();
            } catch (JaloSecurityException e) {
                e.printStackTrace();
            }
            assert messageText != null;
            if (messageText.length() < 100) {
                return messageText;
            } else return messageText.substring(0, 101);
        } else return titleText;
    }

    @Override
    public String exportValue(Object o) throws JaloInvalidParameterException {
        return o == null ? "" : o.toString();
    }
}
