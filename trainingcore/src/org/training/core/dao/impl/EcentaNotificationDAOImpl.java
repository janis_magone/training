package org.training.core.dao.impl;

import de.hybris.platform.b2b.jalo.B2BCustomer;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.training.core.dao.EcentaNotificationDAO;
import org.training.core.jalo.EcentaNotification;
import org.training.core.model.EcentaNotificationModel;

import java.util.*;

public class EcentaNotificationDAOImpl implements EcentaNotificationDAO {

    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<EcentaNotificationModel> findAllNotificationsOlderThanSpecifiedDays(Date oldDate) {

        final StringBuilder query = new StringBuilder("SELECT {pk} FROM {EcentaNotification} WHERE {date}<=?oldDate");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("oldDate", oldDate);

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(EcentaNotificationModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    @Override
    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomer(B2BCustomer b2BCustomer) {
        final StringBuilder query = new StringBuilder("SELECT {pk} FROM {EcentaNotification} WHERE {b2bCustomer}=?b2bCustomer");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("b2bCustomer", b2BCustomer.getCustomerID());

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(EcentaNotificationModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    @Override
    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndHighPriority(B2BCustomer b2BCustomer) {
        final StringBuilder query = new StringBuilder("SELECT {e.pk}\n" +
                "FROM {EcentaNotification as e\n" +
                "JOIN enumerationvalue as enum on {e.priority} \n" +
                "LIKE CONCAT(',#2,', CONCAT({enum.pk}, '%'))}\n" +
                "WHERE {enum.code} = ?highPriority AND {b2bCustomer}=?b2bCustomer\"");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("b2bCustomer", b2BCustomer.getCustomerID());
        params.put("highPriority", "High");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(EcentaNotificationModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    @Override
    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndOrderManagementType(B2BCustomer b2BCustomer) {
        final StringBuilder query = new StringBuilder("SELECT {e.pk}\n" +
                "FROM {EcentaNotification as e\n" +
                "JOIN enumerationvalue as enum on {e.type} \n" +
                "LIKE CONCAT(',#2,', CONCAT({enum.pk}, '%'))}\n" +
                "WHERE {enum.code} = ?orderManagement AND {b2bCustomer}=?b2bCustomer\"\n");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("b2bCustomer", b2BCustomer.getCustomerID());
        params.put("orderManagement", "OrderManagement");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(EcentaNotificationModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    @Override
    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndNewsType(B2BCustomer b2BCustomer) {
        final StringBuilder query = new StringBuilder("SELECT {e.pk}\n" +
                "FROM {EcentaNotification as e\n" +
                "JOIN enumerationvalue as enum on {e.type} \n" +
                "LIKE CONCAT(',#2,', CONCAT({enum.pk}, '%'))}\n" +
                "WHERE {enum.code} = ?news AND {b2bCustomer}=?b2bCustomer\"\n");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("b2bCustomer", b2BCustomer.getCustomerID());
        params.put("news", "News");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(EcentaNotificationModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    @Override
    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndServiceTicketsType(B2BCustomer b2BCustomer) {
        final StringBuilder query = new StringBuilder("SELECT {e.pk}\n" +
                "FROM {EcentaNotification as e\n" +
                "JOIN enumerationvalue as enum on {e.type} \n" +
                "LIKE CONCAT(',#2,', CONCAT({enum.pk}, '%'))}\n" +
                "WHERE {enum.code} = ?serviceTickets AND {b2bCustomer}=?b2bCustomer\"\n");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("b2bCustomer", b2BCustomer.getCustomerID());
        params.put("serviceTickets", "ServiceTickets");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(EcentaNotificationModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }

    @Override
    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndWorkflowType(B2BCustomer b2BCustomer) {
        final StringBuilder query = new StringBuilder("SELECT {e.pk}\n" +
                "FROM {EcentaNotification as e\n" +
                "JOIN enumerationvalue as enum on {e.type} \n" +
                "LIKE CONCAT(',#2,', CONCAT({enum.pk}, '%'))}\n" +
                "WHERE {enum.code} = ?workflow AND {b2bCustomer}=?b2bCustomer\"\n");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("b2bCustomer", b2BCustomer.getCustomerID());
        params.put("workflow", "Workflow");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(EcentaNotificationModel.class));
        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
        return searchResult.getResult();
    }


    public FlexibleSearchService getFlexibleSearchService()
    {
        return flexibleSearchService;
    }


    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
    {
        this.flexibleSearchService = flexibleSearchService;
    }
}
