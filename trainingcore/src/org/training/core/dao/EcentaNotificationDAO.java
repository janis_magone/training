package org.training.core.dao;

import de.hybris.platform.b2b.jalo.B2BCustomer;
import org.training.core.jalo.EcentaNotification;
import org.training.core.model.EcentaNotificationModel;

import java.util.Date;
import java.util.List;

public interface EcentaNotificationDAO {
    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomer(final B2BCustomer b2BCustomer);

    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndHighPriority(final B2BCustomer b2BCustomer);

    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndOrderManagementType(final B2BCustomer b2BCustomer);

    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndNewsType(final B2BCustomer b2BCustomer);

    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndServiceTicketsType(final B2BCustomer b2BCustomer);

    public List<EcentaNotificationModel> findAllNotificationsForSpecificCustomerAndWorkflowType(final B2BCustomer b2BCustomer);

    public List<EcentaNotificationModel> findAllNotificationsOlderThanSpecifiedDays(final Date oldDate);
}
