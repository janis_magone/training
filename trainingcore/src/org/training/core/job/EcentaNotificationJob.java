package org.training.core.job;


import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.EcentaNotificationCronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.training.core.dao.EcentaNotificationDAO;
import org.training.core.jalo.EcentaNotification;
import org.training.core.model.EcentaNotificationModel;
import reactor.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

public class EcentaNotificationJob extends AbstractJobPerformable<EcentaNotificationCronJobModel> {

    @Autowired
    private EcentaNotificationDAO ecentaNotificationDao;

    private final static Logger LOG = Logger.getLogger(EcentaNotificationJob.class.getName());

    @Override
    public PerformResult perform(EcentaNotificationCronJobModel ecentaNotificationCronJobModel) {

        final Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.YEAR, -1);
        Date oldDate = cal.getTime();
        //List<EcentaNotificationModel> notificationsToDelete = new ArrayList<>();
        List<EcentaNotificationModel> notificationModelList = ecentaNotificationDao.findAllNotificationsOlderThanSpecifiedDays(oldDate);

        LOG.debug("List size of objects to mark read and deleted: " + notificationModelList.size());
        for (EcentaNotificationModel ecentaNotificationModel : notificationModelList) {
            ecentaNotificationModel.setRead(Boolean.TRUE);
            ecentaNotificationModel.setDeleted(Boolean.TRUE);
            LOG.debug("Updated notification object: " + ecentaNotificationModel.getId());
        }
        LOG.debug("Finished.");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }
}
